﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen
{
    class FullscreenProcessWindowModifier : IProcessWindowModifier
    {
        /// <summary>
        /// Removes the window's frame and set its position and size to the dimensions of the display monitor that contains it.
        /// </summary>
        /// <param name="window"></param>
        public void Modify(ProcessWindow window)
        {
            var area = window.GetDisplayMonitor().PositionAndSize;

            window.RemoveFrame();

            window.SetPositionAndSize(
                area.Left,
                area.Top,
                area.Width,
                area.Height);
        }
    }
}
