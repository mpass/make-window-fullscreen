﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen
{
    public interface IProcessWindowModifier
    {
        void Modify(ProcessWindow window);
    }
}
