﻿using System;
using System.Diagnostics;

namespace MakeWindowFullscreen
{
    class ProcessViewModel
    {
        public int ProcessId { get; private set; }

        public string ProcessName { get; private set; }

        public string ExecutablePath { get; private set; }

        public ProcessViewModel(int id, string name, string executablePath)
        {
            this.ProcessId = id;
            this.ProcessName = name;
            this.ExecutablePath = executablePath;
        }
    }
}
