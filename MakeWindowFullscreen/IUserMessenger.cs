﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen
{
    public interface IUserMessenger
    {
        void ShowError(string message);

        void ShowInfo(string message);
    }
}
