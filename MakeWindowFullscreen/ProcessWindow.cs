﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using MakeWindowFullscreen.WinApi;

namespace MakeWindowFullscreen
{
    public class ProcessWindow
    {
        protected readonly IntPtr hwnd;

        protected ProcessWindow(IntPtr hwnd)
        {
            this.hwnd = hwnd;
        }

        /// <summary>
        /// Gets a list of windows that belong to the specified <paramref name="process"/>.
        /// </summary>
        /// <param name="processId">The Id of the process to which the windows belong.</param>
        /// <returns>A list of windows that belong to the specified <paramref name="process"/>.</returns>
        /// <remarks>
        /// Throws a <see cref="Win32Exception"/> if enumerating the windows fails.
        /// Throws a <see cref="Win32Exception"/> if getting the thread Id and process Id of a window fails.
        /// </remarks>
        public static IEnumerable<ProcessWindow> GetWindowsForProcess(int processId)
        {
            var windows = new List<ProcessWindow>();

            var callback = new SafeNativeMethods.WindowEnumProc((hwnd, filterProcessId) =>
            {
                // get the thread Id and process Id associated with the window
                int windowProcessId;
                var threadId = SafeNativeMethods.GetWindowThreadProcessId(hwnd, out windowProcessId);

                if (threadId == 0)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error(), string.Format("Failed to get the process Id for hwnd {0}.", hwnd));
                }

                if (windowProcessId == filterProcessId)
                {
                    // if this window belongs to the target process, then add it to the list
                    windows.Add(new ProcessWindow(hwnd));
                }

                // proceed to the next window
                return true;
            });

            // execute the callback for all top-level windows
            int result = SafeNativeMethods.EnumWindows(callback, processId);

            if (result == 0)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error(), "Failed to enumerate the windows.");
            }

            return windows;
        }

        /// <summary>
        /// Gets a list of windows that belong to the specified <paramref name="process"/>.
        /// </summary>
        /// <param name="process">The process to which the windows belong.</param>
        /// <returns>A list of windows that belong to the specified <paramref name="process"/>.</returns>
        /// <remarks>
        /// Throws a <see cref="Win32Exception"/> if enumerating the windows fails.
        /// Throws a <see cref="Win32Exception"/> if getting the thread Id and process Id of a window fails.
        /// </remarks>
        public static IEnumerable<ProcessWindow> GetWindowsForProcess(Process process)
        {
            return GetWindowsForProcess(process.Id);
        }

        public DisplayMonitor GetDisplayMonitor()
        {
            return DisplayMonitor.GetFromWindow(this.hwnd);
        }

        /// <summary>
        /// Attempts to adjust the window's style to remove the border, captop, and buttons.
        /// </summary>
        /// <remarks>
        /// Throws <see cref="Win32Exception"/> if getting the window's current style fails.
        /// Throws <see cref="Win32Exception"/> if setting the window's style fails.
        /// </remarks>
        public void RemoveFrame()
        {
            // get the current window style
            var originalWindowStyle = SafeNativeMethods.GetWindowLong(hwnd, WindowSettings.GWL_STYLE);

            if (originalWindowStyle == 0)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error(), string.Format("Failed to get the window style for hwnd {0}.", hwnd));
            }

            // remove the caption, frame, minimize button, maximize button, and close button from the window style
            var modifiedWindowStyle = originalWindowStyle & ~(WindowStyleFlags.WS_CAPTION | WindowStyleFlags.WS_THICKFRAME | WindowStyleFlags.WS_MINIMIZE | WindowStyleFlags.WS_MAXIMIZE | WindowStyleFlags.WS_SYSMENU);

            // if there is a difference in the window style, then proceed to modify the style
            if (modifiedWindowStyle != originalWindowStyle)
            {
                // apply the adjusted window style to the window
                var previousWindowStyle = SafeNativeMethods.SetWindowLong(hwnd, WindowSettings.GWL_STYLE, modifiedWindowStyle);

                if (previousWindowStyle == 0)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error(), string.Format("Failed to set the window style for hwnd {0}.", hwnd));
                }
            }
        }

        /// <summary>
        /// Sets the position and size of the window.
        /// </summary>
        /// <param name="left">The position of the left side of the window as the number of pixels to the right of the left edge of the screen. A negative value will position the left side of the window off of the screen.</param>
        /// <param name="top">The position of the top of the window as the number of pixels down from the top edge of the screen. A negative value will position the top of the window off of the screen.</param>
        /// <param name="width">The width of the window. This value may be greater than the screen width.</param>
        /// <param name="height">The height of the window. This value may be greater than the screen height.</param>
        /// <remarks>
        /// Throws <see cref="ArgumentOutOfRangeException"/> if width is negative.
        /// Throws <see cref="ArgumentOutOfRangeException"/> if height is negative.
        /// Throws <see cref="Win32Exception"/> if setting the window position and size fails.
        /// </remarks>
        public void SetPositionAndSize(int left, int top, int width, int height)
        {
            if (width < 0)
            {
                throw new ArgumentOutOfRangeException("width", "The width cannot be negative.");
            }

            if (height < 0)
            {
                throw new ArgumentOutOfRangeException("height", "The height cannot be negative.");
            }

            //int resultOfSetWindowPos = SafeNativeMethods.SetWindowPos(hwnd, IntPtr.Zero, -borderThickness, -titleBarHeight, screenWidth + (2 * borderThickness), screenHeight + (titleBarHeight + borderThickness), SetWindowPosFlags.SWP_FRAMECHANGED);
            int resultOfSetWindowPos = SafeNativeMethods.SetWindowPos(hwnd, IntPtr.Zero, left, top, width, height, (uint)SetWindowPosFlags.SWP_FRAMECHANGED);

            if (resultOfSetWindowPos == 0)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error(), string.Format("Failed to set the position and size of hwnd {0}.", hwnd));
            }
        }
    }
}
