﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using MakeWindowFullscreen.WinApi;

namespace MakeWindowFullscreen
{
    public class DisplayMonitor
    {
        public IntPtr Handle { get; private set; }

        public string Name { get; private set; }

        public Rectangle PositionAndSize { get; private set; }

        public int Left
        {
            get { return this.PositionAndSize.Left; }
        }

        public int Top
        {
            get { return this.PositionAndSize.Top; }
        }

        public int Width
        {
            get { return this.PositionAndSize.Width; }
        }

        public int Height
        {
            get { return this.PositionAndSize.Height; }
        }

        private static DisplayMonitor CreateFromHandle(IntPtr hMonitor)
        {
            var monitorInfo = new MONITORINFOEX() { cbSize = Marshal.SizeOf(typeof(MONITORINFOEX)) };

            if (!SafeNativeMethods.GetMonitorInfo(hMonitor, ref monitorInfo))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error(), string.Format("Failed to get monitor info for {0}.", hMonitor));
            }

            return new DisplayMonitor
            {
                Handle = hMonitor,
                Name = monitorInfo.DeviceName,
                PositionAndSize = new Rectangle(
                    monitorInfo.rcMonitor.Left,
                    monitorInfo.rcMonitor.Top,
                    monitorInfo.rcMonitor.Right - monitorInfo.rcMonitor.Left,
                    monitorInfo.rcMonitor.Bottom - monitorInfo.rcMonitor.Top)
            };
        }

        public static IEnumerable<DisplayMonitor> GetAllDisplayMonitors()
        {
            List<DisplayMonitor> displayMonitors = new List<DisplayMonitor>();

            var callback = new SafeNativeMethods.MonitorEnumProc((hMonitor, hdcMonitor, lprcMonitor, dwData) =>
            {
                var item = CreateFromHandle(hMonitor);

                displayMonitors.Add(item);

                return true;
            });

            if (!SafeNativeMethods.EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, callback, IntPtr.Zero))
            {
                throw new Win32Exception(Marshal.GetLastWin32Error(), "Failed to enumerate the display monitors.");
            }

            return displayMonitors;
        }

        public static DisplayMonitor GetFromWindow(IntPtr hwnd)
        {
            var hMonitor = SafeNativeMethods.MonitorFromWindow(hwnd, MonitorDefault.MONITOR_DEFAULTTONEAREST);

            if (hMonitor == IntPtr.Zero)
            {
            }

            return CreateFromHandle(hMonitor);
        }
    }
}
