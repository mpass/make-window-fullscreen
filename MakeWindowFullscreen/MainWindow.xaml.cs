﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MakeWindowFullscreen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IUserMessenger messenger;
        IProcessWindowModifier processWindowModifier;

        DispatcherTimer timer;
        ObservableCollection<ProcessViewModel> items;

        public MainWindow(IUserMessenger messenger, IProcessWindowModifier processWindowModifier)
        {
            this.messenger = messenger;
            this.processWindowModifier = processWindowModifier;

            InitializeComponent();

            this.timer = new DispatcherTimer();
            this.timer.Tick += this.Timer_Tick;
            this.timer.Interval = new TimeSpan(0, 0, 1);

            this.items = new ObservableCollection<ProcessViewModel>(GetProcessesSorted().Select(item => CreateProcessViewModel(item)));

            this.ProcessList.ItemsSource = this.items;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.timer.Start();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private static IEnumerable<Process> GetProcessesSorted()
        {
            return Process.GetProcesses().OrderBy(item => item.ProcessName).ThenBy(item => item.Id);
        }

        private static int Compare(Process a, ProcessViewModel b)
        {
            int result;

            result = string.Compare(a.ProcessName, b.ProcessName);
            if (result != 0)
                return result;

            result = a.Id.CompareTo(b.ProcessId);
            if (result != 0)
                return result;

            return 0;
        }

        private ProcessViewModel CreateProcessViewModel(Process process)
        {
            string executablePath;
            try
            {
                executablePath = process.GetImageName();
            }
            catch (Exception ex)
            {
                executablePath = null;

                this.messenger.ShowError(string.Format("Failed to get the executable path for the process named \"{1}\" (Id: {0}):{3}{2}", process.Id, process.ProcessName, ex, Environment.NewLine));
            }

            return new ProcessViewModel(process.Id, process.ProcessName, executablePath);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            var processes = GetProcessesSorted().ToList();

            int indexOfItems = 0, indexOfProcesses = 0;

            while (indexOfItems < this.items.Count && indexOfProcesses < processes.Count)
            {
                var process = processes[indexOfProcesses];
                var item = this.items[indexOfItems];

                int comparison = Compare(process, item);

                if (comparison == 0)
                {
                    // same process, move on
                    indexOfItems++;
                    indexOfProcesses++;
                }
                else if (comparison < 0)
                {
                    // add process
                    this.items.Insert(indexOfItems, CreateProcessViewModel(process));

                    indexOfItems++;
                    indexOfProcesses++;
                }
                else
                {
                    // remove process
                    this.items.RemoveAt(indexOfItems);

                    indexOfProcesses++;
                }
            }

            // remove processes at the end of the list
            while (indexOfItems < this.items.Count)
            {
                this.items.RemoveAt(indexOfItems);

                indexOfItems++;
            }

            // add processes at the end of the list
            while (indexOfProcesses < processes.Count)
            {
                var process = processes[indexOfProcesses];

                this.items.Insert(indexOfItems, CreateProcessViewModel(process));

                indexOfItems++;
                indexOfProcesses++;
            }
        }

        private void MakeSelectedProcessFullscreen()
        {
            foreach (ProcessViewModel item in this.ProcessList.SelectedItems)
            {
                try
                {
                    foreach (var window in ProcessWindow.GetWindowsForProcess(item.ProcessId))
                    {
                        this.processWindowModifier.Modify(window);
                    }
                }
                catch (Exception ex)
                {
                    this.messenger.ShowError(string.Format("Failed to make the window of the process named \"{1}\" (Id: {0}) fullscreen:{3}{2}", item.ProcessId, item.ProcessName, ex, Environment.NewLine));
                }
            }
        }

        private void MakeFullscreen_Click(object sender, RoutedEventArgs e)
        {
            this.MakeSelectedProcessFullscreen();
        }

        private void ProcessList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.MakeSelectedProcessFullscreen();
            }
        }

        private void Menu_File_Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Menu_Action_MakeFullscreen_Click(object sender, RoutedEventArgs e)
        {
            this.MakeSelectedProcessFullscreen();
        }

        private void Menu_Help_About_Click(object sender, RoutedEventArgs e)
        {
            this.messenger.ShowInfo("This feature is not yet implemented.");
        }

        private void Menu_Help_License_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string licenseText;

                var assembly = this.GetType().Assembly;
                using (var rs = assembly.GetManifestResourceStream(assembly.GetName().Name + ".License.txt"))
                {
                    using (var reader = new System.IO.StreamReader(rs))
                    {
                        licenseText = reader.ReadToEnd();
                    }
                }

                var window = new MessageWindow(licenseText, "License");
                window.Width = 600;
                window.Show();
            }
            catch (Exception ex)
            {
                this.messenger.ShowError(string.Format("An error occurred while attempting to retrieve the license text:{1}{0}", ex, Environment.NewLine));
            }
        }
    }
}
