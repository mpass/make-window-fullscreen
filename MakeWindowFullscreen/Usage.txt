﻿
Usage:

{0}

	Displays the user interface of the MakeWindowFullscreen application,
	which displays all running processes.

{0} <process name>

	Makes the windows of all processes with the specified name
	fullscreen on the display monitor that contains each window.

{0} <process name> <left> <top> <width> <height>

	Moves and resizes the window of the first process with
	the specified name to the position and size specified.

{0} /p[rocess] <process name> [/i[ndex] <index of process>]
	[/a[rea] <left> <top> <width> <height>] [/o[utput]] (window | none)

	Moves and resizes the windows of the process or processes with
	the specified name to the position and size specified, or to be
	fullscreen on the display monitor that contains each window.

{0} /? [/o[utput]] (window | none)

	Displays this help information.

{0} /l[icense]

	Displays the software license.


Parameters:

process

	The name of a process to make fullscreen. To view the names of
	running processes, open the user interface of the
	MakeWindowFullscreen application by running the executable
	without any parameters.

index

	The index of the process with the specified name. In case
	there are multiple processes with the same name, this
	parameter allows a specific process to be identified.

area

	The position and size to set for the process's window. If not
	specified, the window is made fullscreen on the display monitor
	that contains it.  When this parameter is specified, it must be
	followed by four numeric values, which specify the desired x
	and y coordinates of the upper-left corner of the window and the
	desired width and height of the window.

output

	The mode in which messages are output to the user. Valid options
	are "window" or "none".
	"Window" means messages will be displayed in a window.
	"None" means messages will not be shown to the user.
