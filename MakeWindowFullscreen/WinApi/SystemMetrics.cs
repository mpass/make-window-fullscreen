﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen.WinApi
{
    enum SystemMetrics
    {
        SM_CXSCREEN = 0,
        SM_CYSCREEN = 1,
    }
}
