﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWindowFullscreen.WinApi
{
    [Flags]
    enum ProcessAccessFlags
    {
        PROCESS_QUERY_LIMITED_INFORMATION = 0x1000,
    }
}
