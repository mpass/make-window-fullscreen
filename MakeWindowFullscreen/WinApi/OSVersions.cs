﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeWindowFullscreen.WinApi
{
    static class OSVersions
    {
        /// <summary>
        /// The QueryFullProcessImageName function is available in Windows Vista and later.
        /// </summary>
        public const int MinimumSupportForQueryFullProcessImageName = 6;

        public static bool IsQueryFullProcessImageNameSupported
        {
            get { return Environment.OSVersion.Version.Major >= MinimumSupportForQueryFullProcessImageName; }
        }
    }
}
