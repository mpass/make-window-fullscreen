﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MakeWindowFullscreen.WinApi
{
    static class MonitorDefault
    {
        public const int MONITOR_DEFAULTTONULL = 0;
        public const int MONITOR_DEFAULTTOPRIMARY = 1;
        public const int MONITOR_DEFAULTTONEAREST = 2;
    }
}
